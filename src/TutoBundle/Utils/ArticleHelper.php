<?php

namespace TutoBundle\Utils;

class ArticleHelper
{
    public function getArticles()
    {
        return $this->getDummyArticles();
    }
    
    public function getArticle($id)
    {
        $articles = $this->getDummyArticles();
        $my_article = NULL;
        foreach ($articles as $article) {
            if ($id == $article['id']) {
                $my_article = $article;
            }
        }
        unset($articles);
        
        return $my_article;
    }

    protected function getDummyArticles()
    {
        $description = "Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. ";
        $articles = [
            [
                'title' => 'Article 1',
                'description' => $description,
                'id' => 1,
            ],
            [
                'title' => 'Article 2',
                'description' => $description,
                'id' => 2,
            ],
            [
                'title' => 'Article 3',
                'description' => $description,
                'id' => 3,
            ],
        ];
        return $articles;
    }
}
