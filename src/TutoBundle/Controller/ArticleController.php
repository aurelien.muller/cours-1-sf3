<?php

namespace TutoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TutoBundle\Utils\ArticleHelper;

class ArticleController extends Controller
{
    public function listAction()
    {
        // La façon correcte et propre de faire cet appel sera
        // indiqué lors du second cours.
        // Ici, le couplage avec ArticleHelper est évidemment fort, et
        // est à proscrire.
        $article_helper = new ArticleHelper();
        $articles = $article_helper->getArticles();

        return $this->render(
            'article/list.html.twig',
            ['articles' => $articles]
        );
    }
    
    public function displayAction($id)
    {
        // La façon correcte et propre de faire cet appel sera
        // indiqué lors du second cours.
        // Ici, le couplage avec ArticleHelper est évidemment fort, et
        // est à proscrire.
        $article_helper = new ArticleHelper();
        $article = $article_helper->getArticle($id);

        return $this->render(
            'article/full.html.twig',
            ['article' => $article]
        );
    }
}
