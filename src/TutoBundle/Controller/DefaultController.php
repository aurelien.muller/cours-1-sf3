<?php

namespace TutoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {   
        // Exemple d'utilisation avec un template présent dans le
        // bundle même.
        return $this->render('TutoBundle:Default:index.html.twig');
    }
}
