# Symfony 3 - Tutoriel

Projet Symfony créé avec les gestionnaires de dépendances :

* Composer
* Bower

A la racine du projet, pour installer les vendors depuis un terminal, lancer :
```
#!sh

bower install
composer install
```


Installer les assets des bundles inclus :
```
#!sh

bin/console assets:install
```


Contient des exemples :

* Routing
* MVC (model sans injection de dépendances)
* Templating Twig avec décomposition de templates
* Intégration de Bootstrap
* Gestion des inclusions de fichiers